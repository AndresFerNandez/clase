/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista.ejercicios;

import java.util.Scanner;
import ufps.util.coleciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class Evaluador_parentesis {

    public static void main(String[] args) throws Exception {
        String cadena;
        System.out.println("Digite una cadena con ()");
        Scanner teclado = new Scanner(System.in);
        cadena = teclado.nextLine();
        System.out.println("Evaluando sus paréntesis: " + evaluador(cadena));

    }

    private static boolean evaluador(String cadena) throws Exception {
        ListaCD<Character> lista = new ListaCD<>();
        int contador = 0;
        if(cadena == null || cadena.length() == 0) throw new Exception("La cadena está´vacía");
        try{
            for(int i = 0; i < cadena.length(); i++){
                lista.insertarFin(cadena.charAt(i));
            }
            
            while(!lista.esVacia()) {
                char c = lista.eliminar(0);
                if(c == ')' && contador == 0) return false;
                
                if(c == '(') contador++;
                if(c == ')') contador--;
            }
            
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return contador == 0;
    }
}
