/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.util.coleciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class TestLista2 {
    
    public static void main(String[] args) {
         ListaS<Integer> numeros=new ListaS();
         
         numeros.insertarFin(1);
         numeros.insertarFin(10);
         numeros.insertarFin(100);
         numeros.insertarFin(1000);
         
         numeros.insertarFin_NO_repetido(23);
         numeros.insertarFin_NO_repetido(1);
         numeros.insertarFin_NO_repetido(283);
         
          System.out.println(numeros.toString());
         System.out.println("Dato en la posición 2:"+numeros.get(2));
         System.out.println("Dato en la posición 2:"+numeros.get(20));
         numeros.set(2,4000);
         System.out.println("Lista con dato cambiado:\n"+numeros.toString());
         
      
         
    }
}
